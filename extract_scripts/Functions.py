import pandas as pd
import numpy as np
import re
from date_extractor import extract_dates
from datetime import datetime
from dateutil.parser import parse
import dateutil.parser
import math
import boto3
import json

json_bucket_loc = 'groundspeed-tesseract-dev'
s3c = boto3.client('s3')
s3 = boto3.resource('s3')

def outliers(column,colName):
    mean = np.mean([x for x in list(column) if str(x) != 'nan'])
    std = np.std([x for x in list(column) if str(x) != 'nan'])
    low = (mean - (2 * std))
    if low < 0:
        low = 0
    high = (mean + (2 * std))
    veryhigh = (mean + (3 * std))
    print(colName)
    return (low,high,veryhigh)

def claim_summary(claim,file,account,assignee,columnsmand,columnsoutlier,columnsadd,thresholdlist):
    row_dict = {}
    row_dict['claimID'] = file
    row_dict['AccountID'] = account
    row_dict['Assignee'] = assignee
    row_df = claim[claim.claimID == file]
    row_dict['Records'] = len(row_df)
    row_dict['Issues'] = []
    row_dict['Priority Issues'] = []
    rowcount = 0
    i = 0
    # row_dict['ClaimCount Issue'] = row_dict[(row_dict['claimIsSummaryCalculated']>0) & (row_dict['B']>0) & (row_dict['claimIsSummaryCalculated']>0)].count()
    row_dict['Scientific in Policy Number'] = row_df.policyNumberCalculated.str.contains(r'E\+').sum()
    row_dict['Scientific in Claim Number'] = row_df.claimNumberCalculated.str.contains(r'E\+').sum()
    for column in row_df:
        count = row_df[column].notnull().sum()
        rowcount = rowcount + count
    row_dict['Data_Points'] = rowcount
    for column in columnsoutlier:
        count = len(row_df[row_df[column] > thresholdlist[i][1]])
        count1 = len(row_df[row_df[column] < thresholdlist[i][0]])
        count2 = len(row_df[row_df[column] > thresholdlist[i][2]])
        indicies = row_df.index[row_df[column] > thresholdlist[i][1]].tolist()
        indicies1 = row_df.index[row_df[column] < thresholdlist[i][0]].tolist()
        indicies2 = row_df.index[row_df[column] > thresholdlist[i][2]].tolist()
        indicies_all = indicies + indicies1 + indicies2
        for item in indicies2:
            tup = (column + ":", "%.2f" % row_df[column][item])
            row_dict['Priority Issues'].append(tup)
        for item in indicies_all:
            tup = (column + ":", "%.2f" % row_df[column][item])
            row_dict["Issues"].append(tup)
        row_dict['Priority'] = count2
        row_dict['Outlier ' + column] = count + count1
        i = i + 1
    for column in columnsmand:
        row_dict['Empty ' + column] = row_df[column].isnull().sum()
    for j in range(0, len(columnsadd)):
        count = 0
        for i in range(0, len(list(row_df[columnsadd[j][2]]))):
            if ((pd.isnull(list(row_df[columnsadd[j][1]])[i]) == False) and (pd.isnull(list(row_df[columnsadd[j][0]])[i]) == False) and (pd.isnull(list(row_df[columnsadd[j][2]])[i]) == False)): #((pd.isnull(list(row_df[columnsadd[j][1]])[i]) == True) and (pd.isnull(list(row_df[columnsadd[j][0]])[i]) == False)) or ((pd.isnull(list(row_df[columnsadd[j][1]])[i]) == False) and (pd.isnull(list(row_df[columnsadd[j][0]])[i]) == True)) or
                if (np.nansum([list(row_df[columnsadd[j][2]])[i]]) * 0.99 > np.nansum([list(row_df[columnsadd[j][0]])[i], list(row_df[columnsadd[j][1]])[i]])) or (np.nansum([list(row_df[columnsadd[j][0]])[i], list(row_df[columnsadd[j][1]])[i]]) > np.nansum([list(row_df[columnsadd[j][2]])[i]])):
                    count = count + 1
        row_dict[columnsadd[j][2] + ' Addition'] = count
    row_dict['Priority Issues'] = list(set(row_dict['Priority Issues']))
    row_dict['Issues'] = list(set(row_dict['Issues']))
    return(row_dict)

def claim_summaryrecord(claim,columnsmand,columnsoutlier,columnsadd,thresholdlist):
	list_dict = []
	for index, row in claim.iterrows():
	    row_dict = {}
	    row_df = row.to_frame().T
	    row_dict['claimID'] = row_df['claimID'].item()
	    row_dict['AccountID'] = row_df['externalAccountID'].item()
	    row_dict['fileID'] = row_df['fileID'].item()
	    row_dict['Assignee'] = row_df['Assignee'].item()

	    row_dict['Issues'] = []
	    row_dict['Priority Score'] = 0
	    row_dict['Data Points'] = row_df.count(axis=1).item()
	    rowcount = 0
	    i = 0

	    if ((len(row_df[row_df['claimIsSummaryCalculated'] == True]) >=1 and len(row_df[row_df['claimCountCalculated'].isnull()])) >= 1) or (len(row_df[row_df['claimIsSummaryCalculated'] == False]) >=1 and len(row_df[~row_df['claimCountCalculated'].isnull()]) > 1):
	        tup = ("claimIsSummaryCalculated"+ ":", 'Error')
	        row_dict["Issues"].append(tup)
	        row_dict['Priority Score'] = row_dict['Priority Score'] + 10
	    # try:
	    #     if len(row_df[row_df['policyNumberCalculated'].str.contains(r'E\+')]) >= 1:
	    #         tup = ("policyNumberCalculated"+ ":", 'Scientific')
	    #         row_dict["Issues"].append(tup)
	    #         row_dict['Priority Score'] = row_dict['Priority Score'] + 8
	    # except:
	    #     pass
	    # try:
	    #     if len(row_df[row_df['claimNumberCalculated'].str.contains(r'E\+')]) >= 1:
	    #         tup = ("claimNumberCalculated"+ ":", 'Scientific')
	    #         row_dict["Issues"].append(tup)
	    #         row_dict['Priority Score'] = row_dict['Priority Score'] + 8
	    # except:
	    #     pass
	    for column in columnsoutlier:
	        indicies = row_df.index[row_df[column] > thresholdlist[i][1]].tolist()
	        indicies1 = row_df.index[row_df[column] < thresholdlist[i][0]].tolist()
	        indicies2 = row_df.index[row_df[column] > thresholdlist[i][2]].tolist()
	        std = (thresholdlist[i][1] - thresholdlist[i][0])/4
	        mean = thresholdlist[i][1] - 2*(std)
	        indicies_all = indicies + indicies1 + indicies2
	        for item in indicies_all:
	            if item in indicies2:
	                tup = (column + " high"+":", "%.2f" % row_df[column][item])
	                row_dict['Issues'].append(tup)
	                row_dict['Priority Score'] = row_dict['Priority Score'] + ((row_df[column][item]-mean)/(std)) * 15
	            else:
	                tup = (column + ":", "%.2f" % row_df[column][item])
	                row_dict["Issues"].append(tup)
	                row_dict['Priority Score'] = row_dict['Priority Score'] + 3
	        i = i + 1
	    for column in columnsmand:
	        if len(row_df[row_df[column].isnull()]) >= 1:
	            tup = (column +":", "Missing")
	            row_dict['Issues'].append(tup)
	            row_dict['Priority Score'] = row_dict['Priority Score'] + 5            
	    for j in range(0, len(columnsadd)):
	        for i in range(0, len(list(row_df[columnsadd[j][2]]))):
	            if ((pd.isnull(list(row_df[columnsadd[j][0]])[i]) == False) and ((list(row_df[columnsadd[j][0]])[i]) != 0) and (list(row_df[columnsadd[j][0]])[i] == list(row_df[columnsadd[j][1]])[i])):
	                tup = (columnsadd[j][0] + ":", " Total Paid = Total Reserve")
	                row_dict["Issues"].append(tup)
	                row_dict['Priority Score'] = row_dict['Priority Score'] + 10
	            if ((pd.isnull(list(row_df[columnsadd[j][2]])[i]) == False)):
	                if (np.nansum([list(row_df[columnsadd[j][2]])[i]]) * 0.95 > np.nansum([list(row_df[columnsadd[j][0]])[i], list(row_df[columnsadd[j][1]])[i]])) or (np.nansum([list(row_df[columnsadd[j][0]])[i], list(row_df[columnsadd[j][1]])[i]]) > np.nansum([list(row_df[columnsadd[j][2]])[i]])):
	                    tup = (columnsadd[j][2] + " Addition" +":", "%.2f" % row_df[columnsadd[j][2]])
	                    row_dict["Issues"].append(tup)
	                    row_dict['Priority Score'] = row_dict['Priority Score'] + ((abs(np.nansum([list(row_df[columnsadd[j][0]])[i], list(row_df[columnsadd[j][1]])[i]]) - np.nansum([list(row_df[columnsadd[j][2]])[i]]))/1000)*10)
	    row_dict['Issues'] = list(set(row_dict['Issues']))
	    row_dict['Issues Length'] = len(list(set(row_dict['Issues'])))
	    list_dict.append(row_dict)
	return list_dict

def exposure_summaryrecord(exposure,columnsmand,columnsoutlier,thresholdlist):
    row_dict = {}
    row_df = exposure
    row_dict['exposureID'] = row_df['exposureID'].item()
    row_dict['AccountID'] = row_df['clientIDRaw'].item()
    row_dict['fileID'] = row_df['fileID'].item()
    row_dict['Issues'] = []
    row_dict['Priority Score'] = 0
    row_dict['Records'] = len(row_df)
    row_dict['Data Points'] = row_df.count(axis=1).item()
    i = 0
    if len(row_df['exposureAddress1Calculated'].str.contains(r'/d+')) < 1:
    	tup = ("exposureAddress1Calculated"+ ":", 'Does not contain numbers')
    	row_dict["Issues"].append(tup)
    	row_dict['Priority Score'] = row_dict['Priority Score'] + 8
    for column in columnsoutlier:
        indicies = row_df.index[row_df[column] > thresholdlist[i][1]].tolist()
        indicies1 = row_df.index[row_df[column] < thresholdlist[i][0]].tolist()
        indicies2 = row_df.index[row_df[column] > thresholdlist[i][2]].tolist()
        std = (thresholdlist[i][1] - thresholdlist[i][0])/4
        mean = thresholdlist[i][1] - 2*(std)
        indicies_all = indicies + indicies1 + indicies2
        for item in indicies_all:
            if item in indicies2:
                tup = (column + " high"+":", "%.2f" % row_df[column][item])
                row_dict['Issues'].append(tup)
                row_dict['Priority Score'] = row_dict['Priority Score'] + ((row_df[column][item]-mean)/(std)) * 15
            else:
                tup = (column + ":", "%.2f" % row_df[column][item])
                row_dict["Issues"].append(tup)
                row_dict['Priority Score'] = row_dict['Priority Score'] + 3
        i = i + 1
    for column in columnsmand:
        if len(row_df[row_df[column].isnull()]) >= 1:
            tup = (column +":", "Missing")
            row_dict['Issues'].append(tup)
            row_dict['Priority Score'] = row_dict['Priority Score'] + 5
    row_dict['Issues'] = list(set(row_dict['Issues']))
    row_dict['Issues Length'] = len(list(set(row_dict['Issues'])))
    return(row_dict)

def policy_summary(policy,columnsmand,columnsoutlier,columnsdate,thresholdlist):
    row_dict = {}
    row_df = policy
    row_dict['policyID'] = row_df['policyID'].item()
    row_dict['AccountID'] = row_df['clientIDRaw'].item()
    row_dict['fileID'] = row_df['fileID'].item()
    row_dict['Priority Score'] = 0
    row_dict["Issues"] = []
    row_dict['Data Points'] = row_df.count(axis=1).item()
    i = 0

    try:
        if len(row_df[row_df['PY_UnderlyingUmbrellaInsuredName'].str.contains(r'\d')]) >= 1:
            tup = ("PY_UnderlyingUmbrellaInsuredName"+ ":", 'Digit in text')
            row_dict["Issues"].append(tup)
            row_dict['Priority Score'] = row_dict['Priority Score'] + 8
    except:
        pass
    try:
        if len(row_df[row_df['PY_InsuredName'].str.contains(r'\d')]) >= 1:
            tup = ("PY_InsuredName"+ ":", 'Digit in text')
            row_dict["Issues"].append(tup)
            row_dict['Priority Score'] = row_dict['Priority Score'] + 8
    except:
        pass
    for column in columnsoutlier:
        indicies = row_df.index[row_df[column] > thresholdlist[i][1]].tolist()
        indicies1 = row_df.index[row_df[column] < thresholdlist[i][0]].tolist()
        indicies2 = row_df.index[row_df[column] > thresholdlist[i][2]].tolist()
        std = (thresholdlist[i][1] - thresholdlist[i][0])/4
        mean = thresholdlist[i][1] - 2*(std)
        indicies_all = indicies + indicies1 + indicies2
        for item in indicies_all:
            if item in indicies2:
                tup = (column + " high"+":", "%.2f" % row_df[column][item])
                row_dict['Issues'].append(tup)
                row_dict['Priority Score'] = row_dict['Priority Score'] + ((row_df[column][item]-mean)/(std)) * 15
            else:
                tup = (column + ":", "%.2f" % row_df[column][item])
                row_dict["Issues"].append(tup)
                row_dict['Priority Score'] = row_dict['Priority Score'] + 3
        i = i + 1
    for column in columnsmand:
        if len(row_df[row_df[column].isnull()]) >= 1:
            tup = (column +":", "Missing")
            row_dict['Issues'].append(tup)
            row_dict['Priority Score'] = row_dict['Priority Score'] + 5
    for column in columnsdate:
        print(row_df[column].item())
        try:
            if is_date(row_df[column].values[0]) == False:
                tup = (column +":", "Not Valid Date")
                row_dict['Issues'].append(tup)
                row_dict['Priority Score'] = row_dict['Priority Score'] + 5
        except:
            pass
    row_dict['Issues'] = list(set(row_dict['Issues']))
    row_dict['Issues Length'] = len(list(set(row_dict['Issues'])))
    return(row_dict)

# Flags files missing numbers or dates in JSON that are located in extract
def flagger(intersect, extract):
    dict_flag = {'FileID':None, 'FLAG':None, 'Columns':None, 'Values':None}
    flag = 'FALSE'
    issue = 'FALSE'
    missing = []
    keys = []
    text = open_json(intersect)
    if text != None:
        numbers = extract_numbers(text)
        dates = date_extract(text)
        row = extract[extract.fileID == intersect]

        for key, value in row.items():
            if 'limit' in key or 'premium' in key or 'retention' in key or 'deductible' in key:
                if len(value) > 1:
                    value = list(value)
                    for j in range(0, len(value)):
                        if not math.isnan(value[j]):
                            value[j] = int(list(row[key].to_dict().values())[j])
                            if str(value[j]) not in numbers and value[j] != 0:
                                flag = 'TRUE'
                                missing.append(value[j])
                                keys.append(key)
                        else:
                            value[j] = list(row[key].to_dict().values())[j]
                else:
                    if not math.isnan(value):
                        value = int(list(row[key].to_dict().values())[0])
                        if str(value) not in numbers and value != 0:
                            flag = 'TRUE'
                            missing.append(value)
                            keys.append(key)
                    else:
                        value = list(row[key].to_dict().values())[0]

            if 'date' in key:
                if len(value) > 1:
                    value = list(value)
                    for k in range(0, len(value)):
                        value[k] = list(row[key].to_dict().values())[k]
                        if type(value[k]) != float:
                            value[k] = datetime.strftime(dateutil.parser.parse(str(value[k])), '%m/%d/%Y')
                            if str(value[k]) not in dates:
                                flag = 'TRUE'
                                missing.append(value[k])
                                keys.append(key)
                        else:
                            value[k] = list(row[key].to_dict().values())[k]
                else:
                    value = list(row[key].to_dict().values())[0]
                    if type(value) != float:
                        value = datetime.strftime(dateutil.parser.parse(value), '%m/%d/%Y')
                        if value not in dates:
                            flag = 'TRUE'
                            missing.append(value)
                            keys.append(key)
                    else:
                        value = list(row[key].to_dict().values())[0]
    else:
        issue = 'TRUE'
        flag = 'N/A'
        keys = 'N/A'
        missing = 'N/A'
    if issue == 'TRUE':
        dict_flag = {'FileID': intersect, 'FLAG': flag, 'Columns': keys, 'Values': missing}
        return dict_flag
    else:
        keys = list(set(keys))
        missing = list(set(missing))
        dict_flag = {'FileID':intersect, 'FLAG':flag, 'Columns':keys, 'Values':missing}
        print(intersect + " Completed")
        return dict_flag

def date_extract(lister):
    dater = []
    datelist = []
    for string in lister:
        dater.append(extract_dates(string))
    for lister in dater:
        for date in lister:
            if date != None:
                item = date.strftime('%m/%d/%Y')
                datelist.append(item)
    datelist = list(set(datelist))
    return datelist

def extract_numbers(lister):
    numbers_list = []
    for string in lister:
        string = string.replace(',','')
        string = string.replace('$','')
        string = string.replace('mm','000000')
        numbers_string = re.findall('(\d+)', string)
        numbers_string1 = re.findall('(\d+[?\.]\d+)', string)
        numbers_list.append(numbers_string)
        numbers_list.append(numbers_string1)
    flat_list = [item for sublist in numbers_list for item in sublist]
    return(list(set(flat_list)))

def open_json(filename):
    try:
        example = s3.Object('groundspeed-tesseract-dev', 'json_out/' + filename + '.json')
        example = example.get()['Body'].read()
        example = json.loads(example)
        length = len(example['pages'])
        text1 = ''
        for i in range(0, length):
            text1 += "Pagez: " + str(i + 1) + '\n'
            text1 += (example['pages'][i]['pageText']) + '\n'
        text1 = text1.split("Pagez:")
        return text1
    except Exception as e:
        print(e, 'Error for '+filename)
        pass

def float_convert(val):
    try:
        val = float(val)
    except ValueError as e:
        val = np.nan
#         print(e)
    return val

def outlier_thresholds(column):
    # Calculates outlier thresholds
    column = list(column)
    mean = np.nanmean(column)
    std = np.nanstd(column)
    low = (mean - (2 * std))
    if low < 0:
        low = 0
    high = (mean + (2 * std))
    return (low,high)

def outlier_check(column):
    columnclean = list(column)
    for i in range(0, len(columnclean)):
        columnclean[i] = float_convert(columnclean[i])
    columnclean = pd.Series(columnclean).astype(float)
    thresholds = outlier_thresholds(columnclean)
    low = thresholds[0]
    high = thresholds[1]
#     print(columnclean)
    outlierlist = []
    for row in columnclean:
        if row < low:
            outlierlist.append('LOW')
        elif row > high:
            outlierlist.append('HIGH')
        else:
            outlierlist.append('GOOD')
    return pd.Series(outlierlist)

def is_date(string):
    try: 
        parse(string)
        return True
    except ValueError:
        return False