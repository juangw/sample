import pandas as pd
import boto3
import io
import json
import re
import math
import numpy as np
import boto.s3.connection
from os import listdir
from os.path import isfile, join
from joblib import Parallel, delayed
from date_extractor import extract_dates
import dateutil.parser
from datetime import datetime
from Functions import open_json, extract_numbers, date_extract, flagger

s3c = boto3.client('s3')
s3 = boto3.resource('s3')

extract_bucket_loc = 'groundspeed-extracts-prod'
extract_prefix_string = 'Aon_Casualty\Extracts_Ready\AON_100_Merge\7-19-17-49-policies.xlsx'
json_bucket_loc = 'groundspeed-tesseract-dev'
json_prefix_string = 'json_out/'

if __name__ == '__main__':

    extract = pd.read_excel('Z:\\williamjuang\\7-19-17-49-policies.xlsx')
    extractfileid = list(set(extract['fileID']))

    mypath = 'X:\json_out'
    onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

    jsonfileid = []
    for f in onlyfiles:
        jsonfileid.append(f.replace('.json', ''))

    intersect = []
    for i in set(extractfileid).intersection(set(jsonfileid)):
        intersect.append(i)
    print('Intersection created')

    flagfiles = []
    flagfiles.extend(Parallel(n_jobs=20)(delayed(flagger)(intersect[i], extract) for i in range(0, len(intersect))))
    df = pd.DataFrame.from_records(flagfiles)
    df.to_csv('JSON_QA.csv')
    print('All Files Checked')
