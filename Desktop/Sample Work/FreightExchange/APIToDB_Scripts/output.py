import json
import re
import datetime
import pymysql
from ftfy import fix_encoding

db = pymysql.connect( host='DESKTOP-29I9KF4', user='root', passwd='#####', db='fe', charset='utf8' )
cur = db.cursor(pymysql.cursors.DictCursor)


def cleanhtml(raw_html):
  cleanr = re.compile('(?i)<(?!img|/img).*?>', re.UNICODE)
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

emoji_pattern = re.compile("["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags=re.UNICODE)

def main():

    with open('output.txt', 'r', encoding='utf-8') as data_file:
        data = json.loads(data_file.read().encode("utf-8"))
        data_length = len(data)
        for j in range (0,data_length):
            my_listoflist = []
            message = data[j]['conversation_message']['body']
            message = cleanhtml(str(fix_encoding(message)))
            message = emoji_pattern.sub(r'', message)  # no emoji
            messageid = data[j]['conversation_message']['id']
            timestamp = datetime.datetime.fromtimestamp(data[j]['created_at']).strftime('%Y-%m-%d %H:%M:%S')
            authorid = data[j]['conversation_message']['author']['id']
            attach = data[j]['conversation_message']['attachments']
            lines = data[j]['conversation_parts']['conversation_parts']
            url = data[j]['conversation_message']['url']
            count = data[j]['conversation_parts']['total_count']
            authortype = data[j]['conversation_message']['author']['type']

            my_list_first = [int(data[j]['id']), timestamp, url, authorid, message, messageid, count, 0]
            my_listoflist.append(my_list_first)

            sql = ("INSERT INTO fe.conversations (CHATID, TIMESTAMPER, URL, AUTHORID, AUTHORTYPE, MESSAGE, MESSAGEID" \
                  ", COUNTS, temp_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)")
            cur.execute(sql, (int(data[j]['id']), timestamp, url, authorid, authortype, str(message), messageid, count, 0))
            db.commit()

            if (data[j]['conversation_parts']['total_count'] >= 1):
                for i in range(data[j]['conversation_parts']['total_count']):
                    if lines[i]['part_type'] != 'close':
                        messages = cleanhtml(str(lines[i]['body']))
                        messages = emoji_pattern.sub(r'', messages)
                        timestamp = datetime.datetime.fromtimestamp(lines[i]['created_at']).strftime('%Y-%m-%d %H:%M:%S')
                        authorid = lines[i]['author']['id']
                        messageid = lines[i]['created_at']
                        authortype = lines[i]['author']['type']

                        my_list = [data[j]['id'], timestamp, url, authorid, messages, messageid, count, 0]
                        my_listoflist.append(my_list)

                        sql = ("INSERT INTO fe.conversations (CHATID, TIMESTAMPER, URL, AUTHORID, AUTHORTYPE, MESSAGE, MESSAGEID" \
                               ", COUNTS, temp_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)")
                        cur.execute(sql, (int(data[j]['id']), timestamp, url, authorid, authortype, str(messages), messageid, count, 0))
                        db.commit()



if __name__ == '__main__':
    main()