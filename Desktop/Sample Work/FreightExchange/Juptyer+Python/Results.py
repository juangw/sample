# Importing modules that are needed for this lambda function
from intercom.client import Client
import json
import pandas as pd
import nltk
from nltk.tokenize import RegexpTokenizer
from stop_words import get_stop_words
from nltk.stem.porter import PorterStemmer
import numpy as np
import requests

# Access token given from Intercom (the one given is for the TEST Freight Exchange page). Can be found at https://app.intercom.com/developers/hsi53gcy/access-token
intercom = Client(personal_access_token='################################')

def processor(text, context):

    # LDA models from the ChatBot Testing.py file. YOU NEED TO COPY THE MODELS AND DATA INTO THE CORRESPONDING FUNCTIONS!
    ldamodelinsur = [(0, '0.012*"com" + 0.011*"can" + 0.008*"insur" + 0.008*"hi" + 0.008*"freightexchang" + 0.007*"thank" + 0.007*"2" + 0.007*"book" + 0.006*"item" + 0.006*"au" + 0.006*"pleas" + 0.006*"quot" + 0.006*"need" + 0.006*"will" + 0.006*"box" + 0.005*"email" + 0.005*"just" + 0.005*"get" + 0.005*"send" + 0.005*"martyn" + 0.005*"us" + 0.004*"http" + 0.004*"2017" + 0.004*"request" + 0.004*"use" + 0.004*"deliveri" + 0.004*"custom" + 0.004*"damag" + 0.004*"photo" + 0.004*"regard" + 0.004*"amp" + 0.004*"attach" + 0.004*"freight" + 0.004*"pick" + 0.004*"t" + 0.003*"rule" + 0.003*"label" + 0.003*"receiv" + 0.003*"price" + 0.003*"see" + 0.003*"cost" + 0.003*"respons" + 0.003*"api" + 0.003*"mail" + 0.003*"www" + 0.003*"x" + 0.003*"one" + 0.003*"also" + 0.003*"know" + 0.003*"png" + 0.003*"shipment" + 0.003*"now" + 0.003*"0" + 0.003*"1" + 0.003*"claim" + 0.003*"static"')]
    ldamodelbook = [(0, '0.021*"book" + 0.014*"number" + 0.014*"can" + 0.014*"com" + 0.012*"pleas" + 0.010*"pick" + 0.010*"hi" + 0.009*"thank" + 0.008*"will" + 0.008*"iren" + 0.007*"freightexchang" + 0.007*"email" + 0.007*"au" + 0.006*"address" + 0.006*"need" + 0.006*"deliveri" + 0.005*"today" + 0.005*"2" + 0.005*"just" + 0.005*"2017" + 0.005*"get" + 0.005*"label" + 0.004*"gt" + 0.004*"lt" + 0.004*"receiv" + 0.004*"one" + 0.004*"us" + 0.004*"custom" + 0.004*"wrote" + 0.004*"t" + 0.004*"good" + 0.004*"collect" + 0.004*"box" + 0.004*"www" + 0.004*"toll" + 0.004*"send" + 0.004*"rose" + 0.004*"may" + 0.003*"call" + 0.003*"day" + 0.003*"time" + 0.003*"know" + 0.003*"confirm" + 0.003*"ye" + 0.003*"mail" + 0.003*"pm" + 0.003*"use" + 0.003*"s" + 0.003*"regard" + 0.003*"attach"')]
    ldamodellabel = [(0, '0.021*"book" + 0.020*"label" + 0.013*"can" + 0.013*"hi" + 0.011*"pick" + 0.010*"will" + 0.010*"pleas" + 0.009*"just" + 0.009*"com" + 0.008*"need" + 0.008*"thank" + 0.006*"get" + 0.006*"address" + 0.006*"ship" + 0.006*"t" + 0.006*"send" + 0.006*"freightexchang" + 0.006*"number" + 0.006*"box" + 0.006*"email" + 0.006*"s" + 0.005*"today" + 0.005*"photo" + 0.005*"receiv" + 0.005*"time" + 0.005*"attach" + 0.004*"2" + 0.004*"quot" + 0.004*"know" + 0.004*"iren" + 0.004*"note" + 0.004*"now" + 0.004*"print" + 0.004*"au" + 0.004*"deliveri" + 0.004*"come" + 0.004*"pickup" + 0.004*"item" + 0.004*"see" + 0.004*"job" + 0.004*"confirm" + 0.003*"2017" + 0.003*"freight" + 0.003*"custom" + 0.003*"one" + 0.003*"ve" + 0.003*"ok" + 0.003*"us" + 0.003*"go" + 0.003*"good"')]
    ldamodelquote = [(0, '0.013*"can" + 0.013*"need" + 0.012*"hi" + 0.012*"thank" + 0.010*"quot" + 0.009*"freight" + 0.008*"will" + 0.008*"x" + 0.008*"pallet" + 0.008*"get" + 0.008*"pick" + 0.007*"send" + 0.007*"ok" + 0.007*"price" + 0.007*"box" + 0.007*"deliveri" + 0.006*"t" + 0.006*"address" + 0.006*"pleas" + 0.005*"com" + 0.005*"1" + 0.005*"just" + 0.005*"depot" + 0.005*"good" + 0.005*"s" + 0.005*"compani" + 0.005*"cost" + 0.004*"au" + 0.004*"tri" + 0.004*"book" + 0.004*"packag" + 0.004*"don" + 0.004*"hello" + 0.004*"one" + 0.004*"want" + 0.004*"item" + 0.004*"look" + 0.004*"now" + 0.004*"carton" + 0.004*"iren" + 0.004*"melbourn" + 0.004*"2" + 0.004*"provid" + 0.003*"like" + 0.003*"use" + 0.003*"weigh" + 0.003*"know" + 0.003*"weight" + 0.003*"lift" + 0.003*"deliv"')]

    dfnonzero3 = [0.058, 0.058, 0.044, 0.044, 0.035, 0.032, 0.03, 0.029, 0.029, 0.028, 0.027, 0.027, 0.026, 0.026, 0.026, 0.025, 0.024, 0.022, 0.021, 0.021, 0.021, 0.021, 0.021, 0.02, 0.02, 0.02, 0.02, 0.02, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.014, 0.014, 0.014, 0.014, 0.013, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.011, 0.011, 0.011, 0.011, 0.011, 0.01, 0.01, 0.01, 0.01, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003]
    dfnonzero1 = [0.709, 0.571, 0.477, 0.377, 0.317, 0.233, 0.225, 0.216, 0.199, 0.164, 0.159, 0.158, 0.156, 0.155, 0.155, 0.15, 0.146, 0.145, 0.143, 0.138, 0.138, 0.137, 0.136, 0.135, 0.129, 0.128, 0.109, 0.109, 0.108, 0.108, 0.103, 0.103, 0.1, 0.099, 0.096, 0.095, 0.094, 0.094, 0.09, 0.086, 0.086, 0.085, 0.084, 0.082, 0.082, 0.081, 0.081, 0.08, 0.08, 0.079, 0.078, 0.078, 0.078, 0.078, 0.077, 0.076, 0.076, 0.076, 0.075, 0.075, 0.075, 0.075, 0.075, 0.075, 0.074, 0.073, 0.073, 0.073, 0.073, 0.071, 0.071, 0.071, 0.071, 0.07, 0.07, 0.07, 0.069, 0.069, 0.069, 0.068, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.066, 0.065, 0.065, 0.065, 0.064, 0.064, 0.061, 0.06, 0.06, 0.06, 0.058, 0.057, 0.056, 0.055, 0.054, 0.051, 0.051, 0.051, 0.051, 0.045, 0.045, 0.044, 0.044, 0.044, 0.043, 0.042, 0.041, 0.039, 0.039, 0.038, 0.038, 0.038, 0.037, 0.037, 0.037, 0.036, 0.036, 0.036, 0.036, 0.036, 0.036, 0.036, 0.035, 0.035, 0.035, 0.034, 0.034, 0.034, 0.034, 0.033, 0.033, 0.033, 0.031, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.029, 0.029, 0.029, 0.028, 0.027, 0.026, 0.026, 0.026, 0.026, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.025, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.024, 0.023, 0.022, 0.022, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.021, 0.019, 0.019, 0.019, 0.019, 0.019, 0.019, 0.019, 0.018, 0.018, 0.017, 0.017, 0.017, 0.017, 0.017, 0.017, 0.016, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003]
    dfnonzero2 = [0.173, 0.13, 0.108, 0.108, 0.091, 0.088, 0.066, 0.064, 0.063, 0.061, 0.059, 0.057, 0.057, 0.057, 0.053, 0.053, 0.053, 0.053, 0.052, 0.052, 0.052, 0.052, 0.052, 0.052, 0.051, 0.051, 0.05, 0.05, 0.049, 0.048, 0.048, 0.048, 0.048, 0.048, 0.048, 0.048, 0.048, 0.048, 0.047, 0.047, 0.047, 0.047, 0.046, 0.045, 0.045, 0.045, 0.045, 0.045, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.044, 0.043, 0.043, 0.043, 0.043, 0.043, 0.043, 0.043, 0.043, 0.043, 0.043, 0.043, 0.042, 0.042, 0.042, 0.041, 0.041, 0.041, 0.041, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.039, 0.039, 0.039, 0.039, 0.039, 0.039, 0.038, 0.038, 0.038, 0.038, 0.038, 0.038, 0.038, 0.038, 0.038, 0.038, 0.038, 0.038, 0.037, 0.037, 0.037, 0.037, 0.037, 0.036, 0.036, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.035, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.034, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.033, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.029, 0.028, 0.028, 0.028, 0.027, 0.027, 0.027, 0.026, 0.025, 0.024, 0.023, 0.023, 0.023, 0.023, 0.023, 0.023, 0.023, 0.023, 0.022, 0.022, 0.022, 0.022, 0.021, 0.021, 0.021, 0.021, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.02, 0.019, 0.019, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.017, 0.017, 0.017, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.016, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.014, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.01, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.005, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003]
    dfnonzero = [0.084, 0.07, 0.063, 0.059, 0.05, 0.048, 0.048, 0.048, 0.046, 0.046, 0.044, 0.044, 0.043, 0.042, 0.042, 0.042, 0.04, 0.037, 0.037, 0.037, 0.036, 0.036, 0.035, 0.034, 0.034, 0.033, 0.033, 0.033, 0.033, 0.032, 0.031, 0.031, 0.03, 0.03, 0.03, 0.03, 0.03, 0.029, 0.028, 0.026, 0.026, 0.026, 0.026, 0.026, 0.026, 0.025, 0.025, 0.025, 0.025, 0.024, 0.024, 0.023, 0.023, 0.023, 0.023, 0.023, 0.023, 0.022, 0.022, 0.022, 0.022, 0.022, 0.022, 0.021, 0.021, 0.021, 0.021, 0.021, 0.02, 0.02, 0.02, 0.02, 0.02, 0.019, 0.019, 0.019, 0.019, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.018, 0.017, 0.017, 0.017, 0.017, 0.017, 0.016, 0.016, 0.016, 0.016, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.015, 0.014, 0.014, 0.014, 0.014, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.013, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.012, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.011, 0.01, 0.01, 0.01, 0.01, 0.01, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.009, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.008, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.007, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.006, 0.005, 0.005, 0.005, 0.005, 0.005, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.003, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002, 0.002]

    # Reading in JSON object sent from Intercom chat webhook and assigning them to variables. Format of the JSON object is like this: https://github.com/thewheat/intercom-webhooks/blob/master/sample_webhook_output/conversation.user.created.txt
    print(text)
    length = len(text['data']['item']['conversation_parts']['conversation_parts'])
    if (length == 0):
      texts = text['data']['item']['conversation_message']['body'].lower()
    else:
      texts = text['data']['item']['conversation_parts']['conversation_parts'][length-1]['body'].lower()
    convoid = text['data']['item']['id']
    email = text['data']['item']['user']['email']
    authortype = text['data']['item']['user']['type']
    userid = text['data']['item']['user']['id']
    conversation = intercom.conversations.find(id=convoid)

    # Renaming attributes used to stem words and get rid of meaningless words 
    tokenizer = RegexpTokenizer(r'\w+')
    en_stop = get_stop_words('en') # create English stop words list
    p_stemmer = PorterStemmer() # Create p_stemmer of class PorterStemmer

    ### Test Topic Functions ###
    #Functions used to score how close the text matches label text
    def testLabel(texts):

        texts1 = []
        
        # loop through document list
        for i in texts:
            # clean and tokenize document string
            raw = i.lower()
            tokens = tokenizer.tokenize(raw)

            # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in en_stop]

            # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

            # add tokens to list
            texts1.append(stemmed_tokens)

        # using lda model from the ChatBot Testing.py file 
        lda = ldamodellabel
        # splits the lda model into a list of tuples containing a score and a word
        splitPlus = lda[0][1].split('+')
        split = []
        for i in range(0, len(splitPlus)):
            split.append(splitPlus[i].split('*'))
            split[i][0] = split[i][0].replace(" ", "")
            split[i][1] = split[i][1].replace(" ", "").replace('"', "")
        scores = []
        for j in range(0, len(texts)):
            score = 0
            for l in range(0, len(texts[j])):
                # if there are 4,5,6, or 7 number digits (booking numbers) then add to label score
                if (texts[j][l].isdigit() and (
                            len(texts[j][l]) == 5 or len(texts[j][l]) == 4 or len(texts[j][l]) == 6 or len(texts[j][l]) == 7)):
                    score = score + 0.005
            for k in range(0, len(split)):
                keyword = split[k][1]
                words = texts1[j]
                # getting rid of unrelated words from lda model and summing up the scores
                if (str(keyword) in list(
                        words) and keyword != "thank" and keyword != "can" and keyword != "http" and keyword != "will"
                    and keyword != "com" and keyword != "hi" and keyword != "iren" and keyword != 's' and keyword != 'ok' and keyword != 'img'
                    and keyword != "t" and keyword != "ye" and keyword != "just" and keyword != "freight" and keyword != "need" and keyword != "now"
                    and keyword != "email" and keyword != "good" and keyword != "freightexchang" and keyword != "2" and keyword != "one"
                    and keyword != 'pick' and keyword != 'ship' and keyword != 'label' and keyword != 'pleas' and keyword != 'thank' and keyword != 'get'
                    and keyword != 'today' and keyword != 'now' and keyword != 'go' and keyword != 'custom' and keyword != 'book' and keyword!='help' and keyword!='au'):
                    score = score + float(split[k][0])
                # adjusted scores for certain keywords
                elif ((str(keyword) in list(words) and keyword == "label")):
                    score = score + 0.03
                elif ((str(keyword) in list(words) and keyword == "book")):
                    score = score + 0.01
                else:
                    score = score + 0
                if k == (len(split) - 1):
                    scores.append(score)
        # returns score of text
        return (scores)

    def testInsur(texts):

        texts2 = []
        for i in texts:
            tokenizer = RegexpTokenizer(r'\w+')
            # clean and tokenize document string
            raw = i.lower()
            tokens = tokenizer.tokenize(raw)
            # create English stop words list
            en_stop = get_stop_words('en')
            # Create p_stemmer of class PorterStemmer
            p_stemmer = PorterStemmer()

            # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in en_stop]

            # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

            # add tokens to list
            texts2.append(stemmed_tokens)

        # uses lda model from script
        lda = ldamodelinsur
        # splits the lda model into a list of tuples containing a score and a word
        splitPlus = lda[0][1].split('+')
        split = []
        for i in range(0, len(splitPlus)):
            split.append(splitPlus[i].split('*'))
            split[i][0] = split[i][0].replace(" ", "")
            split[i][1] = split[i][1].replace(" ", "").replace('"', "")
        scores = []

        # add custom values to lda model
        split.append(['0.007', 'broke'])

        for j in range(0, len(texts2)):
            score = 0
            for k in range(0, len(split)):
                keyword = split[k][1]
                words = texts2[j]
                # getting rid of unrelated words from lda model and summing up the scores
                if (str(keyword) in list(
                        words) and keyword != "thank" and keyword != "can" and keyword != "http" and keyword != "will"
                    and keyword != "com" and keyword != "hi" and keyword != "iren" and keyword != 's' and keyword != 'ok' and keyword != 'img'
                    and keyword != "t" and keyword != "need" and keyword != "martyn" and keyword != "x" and keyword != "au" and keyword != "1"
                    and keyword != "2" and keyword != "just" and keyword != "need" and keyword != "freightexchang" and keyword != "email"
                    and keyword != "pick" and keyword != "us" and keyword != "amp" and keyword != "use" and keyword != "request" and keyword != "get"
                    and keyword != "also" and keyword != "now" and keyword != "see" and keyword != "one" and keyword != "pleas" and keyword != 'us'
                    and keyword != "good" and keyword != 'help' and keyword != 'damag' and keyword != 'claim' and keyword != 'insur' and keyword != 'send'
                    and keyword != 'api' and keyword != 'pleas' and keyword!='2017' and keyword!='0'):
                    score = score + float(split[k][0])
                # adjusted scores for certain keywords
                elif (str(keyword) in list(words) and (keyword == "damag" or keyword == "claim" or keyword == 'insur')):
                    score = score + 0.012
                elif (str(keyword) in list(words) and (keyword == 'book' or keyword == 'quot')):
                    score = score + 0.023
                else:
                    score = score + 0
                if k == (len(split) - 1):
                    scores.append(score)
        return scores

    def testBook(text):
        texts = []

        # loop through document list
        for i in text:
            # clean and tokenize document string
            raw = i.lower()
            tokens = tokenizer.tokenize(raw)

            # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in en_stop]

            # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

            # add tokens to list
            texts.append(stemmed_tokens)

        lda = ldamodelbook # uses lda model from ChatBot Testing.py script
        # splits the lda model into a list of tuples containing a score and a word
        splitPlus = lda[0][1].split('+')
        split = []
        for i in range(0, len(splitPlus)):
            split.append(splitPlus[i].split('*'))
            split[i][0] = split[i][0].replace(" ", "")
            split[i][1] = split[i][1].replace(" ", "").replace('"', "")
        scores = []

        # add custom values into lda model
        split.append(['0.009', 'track'])
        split.append(['0.009', 'cancel'])
        split.append(['0.005', 'check'])

        for j in range(0, len(texts)):
            score = 0
            for l in range(0, len(texts[j])):
                # if there are 4,5,6, or 7 number digits (booking numbers) then add to book score
                if (texts[j][l].isdigit() and (
                            len(texts[j][l]) == 5 or len(texts[j][l]) == 4 or len(texts[j][l]) == 6 or len(texts[j][l]) == 7)):
                    score = score + 0.030
            for k in range(0, len(split)):
                keyword = split[k][1]
                words = texts[j]
                # getting rid of unrelated words from lda model and summing up the scores
                if (str(keyword) in list(words) and keyword != "ok" and keyword != "hello" and keyword != "rose"
                    and keyword != "iren" and keyword != "hi" and keyword != "gt" and keyword != 's' and keyword != 'www' and keyword != 'com'
                    and keyword != "au" and keyword != "thank" and keyword != "can" and keyword != "email" and keyword != "just" and keyword != "2"
                    and keyword != "now" and keyword != "know" and keyword != "lt" and keyword != "gt" and keyword != "s" and keyword != "book"
                    and keyword != "number" and keyword != "toll" and keyword != "wrote" and keyword != "mail" and keyword != "pleas" and keyword != '2017'
                    and keyword != 'call' and keyword != 'time' and keyword != 'may' and keyword != 'will' and keyword != 'need'
                    and keyword != 'help' and keyword!='t' and keyword!='freightexchang' and keyword!='us' and keyword!='one' and keyword!='good' and keyword!='ye'):
                    score = score + float(split[k][0])
                # adjusted scores for certain keywords
                elif (str(keyword) in list(words) and (keyword == "book" or keyword == "number")):
                    score = score + float(split[k][0])
                else:
                    score = score + 0
                if k == (len(split) - 1):
                    scores.append(score)
        return (scores)

    def testGetaQuote(text):

        texts = []

        # loop through document list
        for i in text:
            # clean and tokenize document string
            raw = i.lower()
            tokens = tokenizer.tokenize(raw)

            # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in en_stop]

            # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

            # add tokens to list
            texts.append(stemmed_tokens)

        lda = ldamodelquote # uses lda model from ChatBot Testing.py script
        # splits the lda model into a list of tuples containing a score and a word
        splitPlus = lda[0][1].split('+')
        split = []
        for i in range(0, len(splitPlus)):
            split.append(splitPlus[i].split('*'))
            split[i][0] = split[i][0].replace(" ", "")
            split[i][1] = split[i][1].replace(" ", "").replace('"', "")
        scores = []
        for j in range(0, len(texts)):
            score = 0
            # if 'from' and 'to', 'kg', or two "x"'s in the text then add to getaquote score
            if ('from' in text[j] and 'to' in text[j]):
                score = score + 0.005
            if (('kg' in text[j])):
                score = score + 0.005
            if (text[j].count("x") == 2):
                score = score + 0.005
            for k in range(0, len(split)):
                keyword = split[k][1]
                words = texts[j]
                # getting rid of unrelated words from lda model and summing up the scores
                if (str(keyword) in list(
                        words) and keyword != "hi" and keyword != "ok" and keyword != "can" and keyword != "will"
                    and keyword != "need" and keyword != "s" and keyword != "iren" and keyword != "com" and keyword != "au" and keyword != "thank"
                    and keyword != "hello" and keyword != "just" and keyword != "t" and keyword != "use" and keyword != "know" and keyword != '2'
                    and keyword != '1' and keyword != 'thank' and keyword != "weigh" and keyword != "weight" and keyword != "carton" and keyword != "lift"
                    and keyword != "tri" and keyword != 'email' and keyword!='like' and keyword!='don' and keyword!='one' and keyword!='look'
                    and keyword!='now' and keyword!='pleas' and keyword!='good' and keyword!='want' and keyword!='quot' and keyword!='t' and keyword!="help"):
                    score = score + float(split[k][0])
                # adjusted scores for certain keywords
                elif (str(keyword) in list(words) and (
                                keyword == "weigh" or keyword == "weight" or keyword == "carton" or keyword == "lift")):
                    score = score + float(split[k][0]) + 0.01
                elif (str(keyword) in list(words) and (keyword == "quot")):
                    score = score + 0.021
                elif (str(keyword) in list(words) and (keyword == "pick")):
                    score = score + 0.006
                elif (str(keyword) in list(words) and keyword=='address'):
                    score = score + 0.002
                elif (str(keyword) in list(words) and keyword!='freight'):
                    score = score + 0.004
                else:
                    score = score + 0
                if k == (len(split) - 1):
                    scores.append(score)
        return (scores)

    ###Find Intent Functions###
    # Uses the scores from the testIntent... functions and decides whether we need to respond to the user
    # regarding that intent. These functions set cut offs and input binary values, so we know whether to respond.
    def findIntentINS(message):
        message.append("filler")
        intent = testInsurIntent(message)
        photos = []
        damags = []
        books = []
        insurs = []
        halps = []
        prices = []
        halps = []
        # setting score cut offs for each intent
        for i in range(0, len(intent['PhotoScores'])):
            if (intent['PhotoScores'][i] > 0.006):
                photo = 1
                photos.append(photo)
            else:
                photo = 0
                photos.append(photo)
            if (intent['DamageScores'][i] >= 0.003):
                damag = 1
                damags.append(damag)
            else:
                damag = 0
                damags.append(damag)
            if (intent['BookScores'][i] >= 0.009):
                book = 1
                books.append(book)
            else:
                book = 0
                books.append(book)
            if (intent['HelpScores'][i] > 0.00):
                halp = 1
                halps.append(halp)
            else:
                halp = 0
                halps.append(halp)
            if (intent['PriceScores'][i] >= 0.003):
                price = 1
                prices.append(price)
            else:
                price = 0
                prices.append(price)
            if (intent['InsuranceScores'][i] >= 0.003):
                insur = 1
                insurs.append(insur)
            else:
                insur = 0
                insurs.append(insur)
        # putting binary '0' or '1' values for each sentence's intents so we can return correct response
        intent['Photo'] = photos
        intent['Book'] = books
        intent['Help'] = halps
        intent['Damage'] = damags
        intent['Price'] = prices
        intent['Insurance'] = insurs
        return intent

    def findIntentLB(message):
        message.append("filler")
        intent = testLabelIntent(message)
        photos = []
        books = []
        confirms = []
        halps = []
        # setting score cut offs for each intent
        for i in range(0, len(intent['PhotoScores'])):
            if (intent['PhotoScores'][i] > 0.006):
                photo = 1
                photos.append(photo)
            else:
                photo = 0
                photos.append(photo)
            if (intent['ConfirmScores'][i] > 0.00):
                confirm = 1
                confirms.append(confirm)
            else:
                confirm = 0
                confirms.append(confirm)
            if (intent['BookScores'][i] >= 0.009):
                book = 1
                books.append(book)
            else:
                book = 0
                books.append(book)
            if (intent['HelpScores'][i] > 0.00):
                halp = 1
                halps.append(halp)
            else:
                halp = 0
                halps.append(halp)
        # putting binary '0' or '1' values for each sentence's intents so we can return correct response
        intent['Photo'] = photos
        intent['Book'] = books
        intent['Help'] = halps
        intent['Confirm'] = confirms
        return (intent)

    def findIntentBK(message):
        message.append("filler")
        intent = testBookIntent(message)
        cancels = []
        tracks = []
        books = []
        times = []
        halps = []
        # setting score cut offs for each intent
        for i in range(0, len(intent['TrackScores'])):
            if (intent['TrackScores'][i] > 0.004):
                track = 1
                tracks.append(track)
            else:
                track = 0
                tracks.append(track)
            if (intent['TimeScores'][i] > 0.004):
                time = 1
                times.append(time)
            else:
                time = 0
                times.append(time)
            if (intent['BookScores'][i] >= 0.009):
                book = 1
                books.append(book)
            else:
                book = 0
                books.append(book)
            if (intent['HelpScores'][i] > 0.00):
                halp = 1
                halps.append(halp)
            else:
                halp = 0
                halps.append(halp)
            if (intent['CancelScores'][i] > 0.00):
                cancel = 1
                cancels.append(cancel)
            else:
                cancel = 0
                cancels.append(cancel)
        # putting binary '0' or '1' values for each sentence's intents so we can return correct response
        intent['Track'] = tracks
        intent['Time'] = times
        intent['Book'] = books
        intent['Help'] = halps
        intent['Cancel'] = cancels
        return (intent)

    def findIntentGQ(message):
        message.append("filler")
        intent = testGetaQuoteIntent(message)
        weights = []
        dimens = []
        packs = []
        lifts = []
        locs = []
        halps = []
        # setting score cut offs for each intent
        for i in range(0, len(intent['WeightScores'])):
            if (intent['WeightScores'][i] > 0.004):
                weight = 1
                weights.append(weight)
            else:
                weight = 0
                weights.append(weight)
            if (intent['PackScores'][i] > 0.004):
                pack = 1
                packs.append(pack)
            else:
                pack = 0
                packs.append(pack)
            if (intent['DimenScores'][i] > 0.004):
                dimen = 1
                dimens.append(dimen)
            else:
                dimen = 0
                dimens.append(dimen)
            if (intent['LiftScores'][i] >= 0.004):
                lift = 1
                lifts.append(lift)
            else:
                lift = 0
                lifts.append(lift)
            if (intent['LocationScores'][i] > 0.004):
                loc = 1
                locs.append(loc)
            else:
                loc = 0
                locs.append(loc)
            if (intent['HelpScores'][i] > 0):
                halp = 1
                halps.append(halp)
            else:
                halp = 0
                halps.append(halp)
        # putting binary '0' or '1' values for each sentence's intents so we can return correct response
        intent['Weight'] = weights
        intent['Pack'] = packs
        intent['Dimen'] = dimens
        intent['Lift'] = lifts
        intent['Location'] = locs
        intent['Help'] = halps
        return intent

    ###Test Intents###
    # Inputs scores into each sentence's set intents based upon what keywords from the lda model
    # are found in each sentence. This function is sorted into pre-set intents and each intent is scored
    # based on what keywords are in that sentence
    def testGetaQuoteIntent(result):
        message = []
        for i in range(0, int(len(result))):
            text = result[i]
            message.append(text)
        texts = []

        # loop through document list
        for i in message:
            # clean and tokenize document string
            raw = i.lower()
            tokens = tokenizer.tokenize(raw)

            # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in en_stop]

            # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

            # add tokens to list
            texts.append(stemmed_tokens)

        lda = ldamodelquote
        # splits the lda model into a list of tuples containing a score and a word
        splitPlus = lda[0][1].split('+')
        split = []
        for i in range(0, len(splitPlus)):
            split.append(splitPlus[i].split('*'))
            split[i][0] = split[i][0].replace(" ", "")
            split[i][1] = split[i][1].replace(" ", "").replace('"', "")
        weightscore = []
        packscores = []
        dimenscore = []
        liftscore = []
        assistscore = []
        locscore = []
        for j in range(0, len(texts)):
            weight = 0
            packscore = 0
            dimen = 0
            lift = 0
            loc = 0
            assist = 0
            # custom words to find in message that match to intents such as location, weight, dimensions, and assist and increase the scores for each intent if present
            if (('from' in message[j]) and ('to' in message[j])):
                loc = loc + 0.006
            if (('kg' in message[j] or 'pound' in message[j] or 'pounds' in message[j])):
                weight = weight + 0.005
            if (message[j].count("x") == 2):
                dimen = dimen + 0.005
            if (("assist" in message[j] or "help" in message[j] or "what" in message[j] or "why" in message[j] or "how" in message[j] or "will" in message[j] or "possible" in message[j])):
                assist = assist + 0.002
            for k in range(0, len(split)):
                keyword = split[k][1]
                words = texts[j]
                # words from lda model that increase individual intent scores because the word is related to the intent
                if (str(keyword) in list(words) and (
                                keyword == 'box' or keyword == 'pallet' or keyword == 'carton' or keyword == 'packag')):
                    packscore = packscore + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "dimens" or keyword == "size")):
                    dimen = dimen + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "weigh" or keyword == "weight")):
                    weight = weight + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "lift" or keyword == "tail")):
                    lift = lift + float(split[k][0])
                elif (str(keyword) in list(words) and (
                                    keyword == "commerci" or keyword == "address" or keyword == "pick" or keyword == "site" or keyword == 'depot')):
                    loc = loc + float(split[k][0])

                if k == (len(split) - 1):
                    dimenscore.append(dimen)
                    weightscore.append(weight)
                    packscores.append(packscore)
                    locscore.append(loc)
                    liftscore.append(lift)
                    assistscore.append(assist)
        messages = message
        # scores put into dictionary to be accessed in final part of lambda function to return response
        dictionary = {"Messages": messages, "WeightScores": weightscore, "PackScores": packscores, "DimenScores": dimenscore, "LiftScores": liftscore,
                "LocationScores": locscore, "HelpScores": assistscore}
        return (dictionary)

    def testInsurIntent(result):
        message = []
        for i in range(0, int(len(result))):
            text = result[i]
            message.append(text)
        texts = []

        # loop through document list
        for i in message:
            # clean and tokenize document string
            raw = i.lower()
            tokens = tokenizer.tokenize(raw)

            # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in en_stop]

            # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

            # add tokens to list
            texts.append(stemmed_tokens)

        lda = ldamodelinsur
        # splits the lda model into a list of tuples containing a score and a word
        splitPlus = lda[0][1].split('+')
        split = []
        for i in range(0, len(splitPlus)):
            split.append(splitPlus[i].split('*'))
            split[i][0] = split[i][0].replace(" ", "")
            split[i][1] = split[i][1].replace(" ", "").replace('"', "")
        damagescore = []
        scores = []
        photoscore = []
        insurancescore = []
        bookscore = []
        pricescore = []
        helpscore = []
        assistscore = []
        for j in range(0, len(texts)):
            damage = 0
            photo = 0
            score = 0
            book = 0
            insurance = 0
            assist = 0
            price = 0
            # custom words to find in message that match to intents such as location, weight, dimensions, and assist and increase the scores for each intent if present
            if (("what" in message[j] or "why" in message[j] or "how" in message[j] or "will" in message[j] or "possible" in message[j])):
                assist = assist + 0.002
            for l in range(0, len(texts[j])):
                if (texts[j][l].isdigit() and (
                            len(texts[j][l]) == 5 or len(texts[j][l]) == 4 or len(texts[j][l]) == 6)):
                    book = book + 0.009
            for k in range(0, len(split)):
                keyword = split[k][1]
                words = texts[j]
                # words from lda model that increase individual intent scores because the word is related to the intent
                if (str(keyword) in list(words) and (keyword == 'damag')):
                    damage = damage + float(split[k][0])
                if ("broke" in texts):
                    damage = damage + 0.01
                elif (str(keyword) in list(words) and (keyword == "photo" or keyword == "attach")):
                    photo = photo + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "help" or keyword == "assist")):
                    assist = assist + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "insur" or keyword == 'claim')):
                    insurance = insurance + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "book" or keyword == 'number' or keyword == 'quot')):
                    book = book + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "price" or keyword == 'cost')):
                    price = price + float(split[k][0])

                if k == (len(split) - 1):
                    damagescore.append(damage)
                    photoscore.append(photo)
                    insurancescore.append(insurance)
                    pricescore.append(price)
                    bookscore.append(book)
                    assistscore.append(assist)
        messages = message
        # scores put into dictionary to be accessed in final part of lambda function to return response
        dictionary = {"Messages": messages, "DamageScores": damagescore, "PhotoScores": photoscore, "InsuranceScores": insurancescore, "BookScores": bookscore,
                "PriceScores": pricescore, "HelpScores": assistscore}
        return dictionary

    def testBookIntent(result):
        message = []
        for i in range(0, int(len(result))):
            text = result[i]
            message.append(text)

        texts = []

        # loop through document list
        for i in message:
            # clean and tokenize document string
            raw = i.lower()
            tokens = tokenizer.tokenize(raw)

            # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in en_stop]

            # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

            # add tokens to list
            texts.append(stemmed_tokens)

        lda = ldamodelbook
        # splits the lda model into a list of tuples containing a score and a word
        splitPlus = lda[0][1].split('+')
        split = []
        for i in range(0, len(splitPlus)):
            split.append(splitPlus[i].split('*'))
            split[i][0] = split[i][0].replace(" ", "")
            split[i][1] = split[i][1].replace(" ", "").replace('"', "")
        split.append(['0.015', 'track'])
        split.append(['0.009', 'cancel'])
        trackscore = []
        scores = []
        timescore = []
        assistscore = []
        bookscore = []
        cancelscore = []
        for j in range(0, len(texts)):
            track = 0
            time = 0
            book = 0
            assist = 0
            score = 0
            cancel = 0
            # custom words to find in message that match to intents such as location, weight, dimensions, and assist and increase the scores for each intent if present
            if (("what" in message[j] or "why" in message[j] or "how" in message[j] or "will" in message[j] or "possible" in message[j])):
                assist = assist + 0.002
            for l in range(0, len(texts[j])):
                if (texts[j][l].isdigit() and (
                            len(texts[j][l]) == 5 or len(texts[j][l]) == 4 or len(texts[j][l]) == 6)):
                    book = book + 0.009
            for k in range(0, len(split)):
                keyword = split[k][1]
                words = texts[j]
                # words from lda model that increase individual intent scores because the word is related to the intent
                if (str(keyword) in list(words) and (keyword == 'track' or keyword == "confirm")):
                    track = track + float(split[k][0])
                elif (str(keyword) in list(words) and (
                                keyword == "time" or keyword == "date" or keyword == "day" or keyword == 'pm' or keyword == 'today')):
                    time = time + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "cancel")):
                    cancel = cancel + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "book" or keyword == 'number')):
                    book = book + 0.004
                elif (str(keyword) in list(words) and (keyword == "assist" or keyword == 'help')):
                    assist = assist + float(split[k][0])

                if k == (len(split) - 1):
                    trackscore.append(track)
                    timescore.append(time)
                    bookscore.append(book)
                    assistscore.append(assist)
                    cancelscore.append(cancel)
        messages = message
        # scores put into dictionary to be accessed in final part of lambda function to return response
        dictionary = {"Messages": messages, "TrackScores": trackscore, "TimeScores": timescore, "BookScores": bookscore, "HelpScores": assistscore,
                "CancelScores": cancelscore}
        return (dictionary)

    def testLabelIntent(result):
        message = []
        for i in range(0, int(len(result))):
            text = result[i]
            message.append(text)

        texts = []

        # loop through document list
        for i in message:
            # clean and tokenize document string
            raw = i.lower()
            tokens = tokenizer.tokenize(raw)

            # remove stop words from tokens
            stopped_tokens = [i for i in tokens if not i in en_stop]

            # stem tokens
            stemmed_tokens = [p_stemmer.stem(i) for i in stopped_tokens]

            # add tokens to list
            texts.append(stemmed_tokens)

        lda = ldamodellabel
        # splits the lda model into a list of tuples containing a score and a word
        splitPlus = lda[0][1].split('+')
        split = []
        for i in range(0, len(splitPlus)):
            split.append(splitPlus[i].split('*'))
            split[i][0] = split[i][0].replace(" ", "")
            split[i][1] = split[i][1].replace(" ", "").replace('"', "")
        bookscore = []
        photoscore = []
        assistscore = []
        confirmscore = []
        for j in range(0, len(texts)):
            book = 0
            photo = 0
            assist = 0
            confirm = 0.0
            # custom words to find in message that match to intents such as location, weight, dimensions, and assist and increase the scores for each intent if present
            if (("what" in message[j] or "why" in message[j] or "how" in message[j] or "will" in message[j])):
                assist = assist + 0.002
            for l in range(0, len(texts[j])):
                if (texts[j][l].isdigit() and (
                            len(texts[j][l]) == 5 or len(texts[j][l]) == 4 or len(texts[j][l]) == 6)):
                    book = book + 0.009
            for k in range(0, len(split)):
                keyword = split[k][1]
                words = texts[j]
                # words from lda model that increase individual intent scores because the word is related to the intent
                if (str(keyword) in list(words) and (keyword == 'book' or keyword == "number")):
                    book = book + float(split[k][0])
                if (str(keyword) in list(words) and (keyword == 'assist' or keyword == "help")):
                    assist = assist + float(split[k][0])
                elif (str(keyword) in list(words) and (
                                keyword == "attach" or keyword == "photo" or keyword == "receiv" or keyword == 'send')):
                    photo = photo + float(split[k][0])
                elif (str(keyword) in list(words) and (keyword == "confirm")):
                    confirm = confirm + float(split[k][0])

                if k == (len(split) - 1):
                    bookscore.append(book)
                    assistscore.append(assist)
                    photoscore.append(photo)
                    confirmscore.append(confirm)
        messages = message
        # scores put into dictionary to be accessed in final part of lambda function to return response
        dictionary = {"Messages": messages, "BookScores": bookscore, "PhotoScores": photoscore, 
                      "ConfirmScores": confirmscore, "HelpScores": assistscore}
        return (dictionary)


    ## This part of the function is used to decide which topic the message should be sorted into by their scores from the test functions
    ## Each score is calculated by going through the test function, and then subtracting it by the median of the test data set then divided by the standard deviation
    ## The score that is higest (in the decider list) is taken as the topic
    for i in range(0, 1):
        decider = []
        scores = []
        
        textlist = []
        lbmedian = 0.017096
        lbstd = 0.01
        textlist.append(texts)
        print(textlist)
        lb = (float(testLabel(textlist)[0]) - lbmedian)/lbstd
        scores.append(testLabel(textlist)[0])
        decider.append(lb)

        bkmedian = 0.061312
        bkstd = 0.021
        print(testBook(textlist)[0])
        bk = (float(testBook(textlist)[0]) - (bkmedian - 0.02))/bkstd
        scores.append(testBook(textlist)[0])
        decider.append(bk)

        insmedian = 0.008855
        insstd = 0.006
        insur = (float(testInsur(textlist)[0]) - insmedian)/insstd
        scores.append(testInsur(textlist)[0])
        decider.append(insur)

        gqmedian = 0.013212
        gqstd = 0.0085
        gq = (float(testGetaQuote(textlist)[0]) - (gqmedian))/gqstd
        scores.append(testGetaQuote(textlist)[0])
        decider.append(gq)

        gqres = []
        lbres = []
        bkres = []
        insres = []
        response = []
        print(decider)
        print(scores)
        scoreslen = len(scores)
        allzeros = False
        index = 1000
        zeros = 0
        other = 0

        ## If all but one of the raw test scores are 0 (in the scores list), then the topic with the score that is not zero is taken as the topic
        for i in range(0,scoreslen):
            if scores[i] == 0:
               zeros = zeros + 1
            else:
               other = other + 1
               if scores[i] > 0.004:
                  index = i
            if zeros == scoreslen-1:
               allzeros=True

        print(decider.index(max(decider)))

        ## This part of the function returns the responses based on the binary results from the findIntent... functions. These are all hard coded responses and can be changed
        ## '0' means there is not information given and '1' means there is information given
        if ((decider.index(max(decider)) == 0 and zeros!=4) or (allzeros==True and index==0)):
            lbres.append(str([texts]).replace("[", "").replace("]", "").replace("'", ""))
            lbres.append("Filler")
            response.append("Label")
            print(findIntentLB(lbres))
            if (findIntentLB(lbres)['Help'][0] == 1):
                response.append("You can find your labels in the 'Manage Bookings' screen under your account")
                response.append("https://www.freightexchange.com.au/staticPage/faq#labels")
            if (findIntentLB(lbres)['Book'][0] == 0):
                response.append("Could I have your booking number please?")
            elif (findIntentLB(lbres)['Book'][0] == 1):
                response.append("Thank you for providing your booking number")
            if (findIntentLB(lbres)['Photo'][0] == 0):
                response.append("Before we generate and send your label, could you provide us with a picture of your fully packed item(s)?")
            if (findIntentLB(lbres)['Photo'][0] == 1):
                response.append("Please have a look in junk mail or spam folder of your inbox in case our emails have fallen in there! Click the ‘Open Shipping label” orange icon on the right hand side of the page, then print or download the file to your computer to print the Shipping Labels and Consignment Notes")
            
            # returns string to intercom as text
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"intercom_user_id": str(userid), "body": "\n".join(response), "type": str(authortype),
                        "message_type": "comment", "admin_id": "820053"})
            # marks conversation as unassigned
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"body": "Reassign", "type": str(authortype), "id": str(convoid),
                        "message_type": "assignment", "assignee_id": "0"})
            return response
        elif ((decider.index(max(decider)) == 1 and zeros!=4) or (allzeros==True and index==1)):
            bkres.append(str([texts]).replace("[", "").replace("]", "").replace("'", ""))
            bkres.append("Filler")
            response.append("Booking")
            print(findIntentBK(bkres))
            if (findIntentBK(bkres)['Help'][0] == 1):
                response.append("https://www.freightexchange.com.au/staticPage/faq#delivery")
            if (findIntentBK(bkres)['Cancel'][0] == 1):
                response.append("The next available agent will cancel your booking. In the meantime, do you mind telling us why you want to cancel your booking?")
            if (findIntentBK(bkres)['Track'][0] == 1):
                response.append("The next available agent will track your package for you.")
            if (findIntentBK(bkres)['Book'][0] == 0):
                response.append("Could I have your booking number please?")
            elif (findIntentBK(bkres)['Book'][0] == 1):
                response.append("Thank you for providing your booking number. We will be with you shortly")
            
            # returns string to intercom as text
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"intercom_user_id": str(userid), "body": "\n".join(response), "type": str(authortype),
                        "message_type": "comment", "admin_id": "820053"})
            # marks conversation as unassigned
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"body": "Reassign", "type": str(authortype), "id": str(convoid),
                        "message_type": "assignment", "assignee_id": "0"})
            return response
        elif ((decider.index(max(decider)) == 2 and zeros!=4) or (allzeros==True and index==2)):
            insres.append(str([texts]).replace("[", "").replace("]", "").replace("'", ""))
            insres.append("Filler")
            response.append('Insurance')
            print(findIntentINS(insres))
            if ((findIntentINS(insres)['Help'][0] == 1) or (findIntentINS(insres)['Insurance'][0] == 1)):
                response.append("https://www.freightexchange.com.au/staticPage/faq#insurance")
            if ((findIntentINS(insres)['Price'][0] == 1)):
                response.append("https://www.freightexchange.com.au/staticPage/faq#fees")
            if (findIntentINS(insres)['Book'][0] == 0):
                response.append("Could I have your booking number please?")
            elif (findIntentINS(insres)['Book'][0] == 1):
                response.append("Thank you for providing your booking number. We will be with you shortly")
            if (findIntentINS(insres)['Photo'][0] == 0):
                response.append(
                    "In order to complete a insurance claim, we need some pictures and information: picture of your packaged good before they were sent, picture of the damaged goods, invoice for replacement or repair, and the BSB and account number as soon as possible")
            if (findIntentINS(insres)['Damage'][0] == 1):
                response.append('https://www.freightexchange.com.au/staticPage/faq#damagedGoods')

            # returns string to intercom as text
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"intercom_user_id": str(userid), "body": "\n".join(response), "type": str(authortype),
                        "message_type": "comment", "admin_id": "820053"})
            # marks conversation as unassigned
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"body": "Reassign", "type": str(authortype), "id": str(convoid),
                        "message_type": "assignment", "assignee_id": "0"})
            return response
        elif ((decider.index(max(decider)) == 3 and zeros!=4) or (allzeros==True and index==3)):
            gqres.append(str([texts]).replace("[", "").replace("]", "").replace("'", ""))
            gqres.append("Filler")
            response.append('GetaQuote')
            print(findIntentGQ(gqres))
            if (findIntentGQ(gqres)['Help'][0] == 1):
               response.append("https://demo.freightexchange.com.au/staticPage/faq#quotes")
            if (findIntentGQ(gqres)['Location'][0] == 0):
               response.append("Where is your item being shipped to and from?")
            if (findIntentGQ(gqres)['Weight'][0] == 0):
               response.append("Can you please provide the total weight of your items in kg?")
            if (findIntentGQ(gqres)['Dimen'][0] == 0):
               response.append("What are the dimensions of your fully packaged object?")
            if (findIntentGQ(gqres)['Pack'][0] == 0):
               response.append("Can you please provide us with how your item will be packaged?")
            if (findIntentGQ(gqres)['Lift'][0] == 0):
               response.append("Will you need tail lift service for your items at either the pick up or delivery location?")
            response.append('Once you provide this information the next available agent will return your quote')
            if len(response) > 4:
               response = []
               response.append("For instant quote please visit our online quote form at https://www.freightexchange.com.au/showQuoteForm")

            # returns string to intercom as text
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"intercom_user_id": str(userid), "body": "\n".join(response), "type": str(authortype),
                        "message_type": "comment", "admin_id": "820053"})
            # marks conversation as unassigned
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"body": "Reassign", "type": str(authortype), "id": str(convoid),
                        "message_type": "assignment", "assignee_id": "0"})
            return response
        if ((texts.lower()=="<p>hi</p>") or (texts.lower()=="<p>hello</p>") or (texts.lower()=="<p>hey</p>")):
            # returns string to intercom as text
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"intercom_user_id": str(userid), "body": "Hello! There are currently no operators to help at the moment. We will be with you as soon as possible", "type": str(authortype),
                        "message_type": "comment", "admin_id": "820053"})
            # marks conversation as unassigned
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"body": "Reassign", "type": str(authortype), "id": str(convoid),
                        "message_type": "assignment", "assignee_id": "0"})
            return 'Greeting'
        # for i in range(0, len(texts.split)):
        #   if ("api" in texts.split[i].lower()):
        #       response.append("We do offer API integration for automated quotes and bookings. For automated quotes, please review the documentation and let me know if you have any questions?")
        #       response.append("https://dev.freightexchange.com.au/")
        #       # returns string to intercom as text
        #       requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
        #                   "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
        #                   data={"intercom_user_id": str(userid), "body": "\n".join(response), "type": str(authortype),
        #                   "message_type": "comment", "admin_id": "820053"})
        # for i in range(0, len(texts.split)):
        #   if ("household" in texts.split[i].lower()):
        #       response.append("We do offer API integration for automated quotes and bookings. For automated quotes, please review the documentation and let me know if you have any questions?")
        #       response.append("https://dev.freightexchange.com.au/")
        #       # returns string to intercom as text
        #       requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
        #                   "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
        #                   data={"intercom_user_id": str(userid), "body": "We can ship household goods only if they are packaged carefully in a crate, carton or pallet.", "type": str(authortype),
        #                   "message_type": "comment", "admin_id": "820053"})
        else:
            print("got nothing")
            # marks conversation as unassigned
            requests.post("https://api.intercom.io/conversations/" + str(convoid) + "/reply", headers={"Accept": "application/json",
                        "Authorization": "Bearer dG9rOmIzNmZlYWY3XzVmZDlfNDlhNF84MDMwX2MxYzM0MWQ2MmY5MToxOjA="},
                        data={"body": "Reassign", "type": str(authortype), "id": str(convoid),
                        "message_type": "assignment", "assignee_id": "0"})
            return 'Do nothing'


