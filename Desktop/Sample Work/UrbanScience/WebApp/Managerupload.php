<?php
    session_start();
    require_once "sqlsrv.php";

    if ( isset($_POST['cancel'] ) ) {
        header("Location: Manager.php");
        return;
    }

    $sql = "SELECT * FROM POPrequestLog_ForWill WHERE id = ?";
	$params = array($_GET['id']);
    $stmt = sqlsrv_query($conn, $sql, $params);
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		if ($row['uploadDate'] == NULL){
			$udate = "No Upload Date";
		}else{
			$udate = htmlentities($row['uploadDate']->format('Y-m-d'));
		}
		$auditor = htmlentities($row['auditAssignment']);
		$bac = htmlentities($row['bac']);
		$division = htmlentities($row['division']);
		$id = htmlentities($row['id']);
    }
    
    if (isset($_POST['upload'])){
    	$sql = "UPDATE POPrequestLog_ForWill SET uploadDate = ? WHERE id = ?";
    	$params = array($_POST['udate'], $_GET['id']);
    	$stmt = sqlsrv_query($conn, $sql, $params);
    	$_SESSION['success'] = 'Package Uploaded';
    	header("Location: Manager.php");
    	return;
    } 

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <title>Upload</title>
</head>
<body>
	<h1>Manager Upload</h1>
	<?php
		if ( isset($_SESSION['error']) ) {
    		echo('<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n");
    		unset($_SESSION['error']);
		}
	?>
	<form method="POST">
		<fieldset>
			<legend>Package Upload</legend>
				<p>Auditor:
				<input type="text" name="auditor" size="60" value="<?= $auditor ?>"/></p>
				<p>BAC:
				<input type="text" name="bac" size="60" value="<?= $bac ?>"/></p>
				<p>Division:
				<input type="text" name="division" size="60" value="<?= $division ?>"/></p>
				<script>
                    $( function() {
                        $( "#udate" ).datepicker({ dateFormat: 'yy-mm-dd'});
                    } );
                </script>
				<p>Upload Date:
				<input type="text" name="udate" size="60" value="<?= $udate ?>" id="udate"/></p>
				<input type="hidden" name="id" value="<?= $id ?>">
				<input type="submit" name="upload" value="Upload">
				<input type="submit" name="cancel" value="Cancel">
		</fieldset>
	</form>
</body>
</html>