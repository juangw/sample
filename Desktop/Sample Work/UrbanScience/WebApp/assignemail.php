<?php
	session_start();
	require_once "sqlsrv.php";

	$email = '';
	$sql = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
	$params = array($_POST['Auditors']);
	$stmt = sqlsrv_query($conn, $sql, $params);
	while (($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) ){
		$email = $row['Email'];
	}

	if ( isset($_POST['Auditors']) && isset($_POST['bac'])
    && isset($_POST['ddate']) && isset($_POST['division'])){
    	$sql1 = "UPDATE POPrequestLog_ForWill SET auditAssignment = ?
	            WHERE id = ?";
	    $params1 = array($_POST['Auditors'], $_SESSION['id']);
	    $stmt1 = sqlsrv_query($conn, $sql1, $params1);
    }

	$to = $email;
	$subject = 'Audit Assigned';
	$msg = 'The MarketPlus Package with BAC number ' . $_POST['bac'] . ' ' . $_POST['division'] . ' has been assigned to you, and this package is due ' . $_POST['ddate'] . '.';
	$from = 'kyle.hopper@channelvantage.com';
	$headers = 'From:'.$from;
		
	mail($to, 'Audit Assigned', $msg, $headers);

	echo "Email sent";
?>
