<?php
    session_start();
    require_once "sqlsrv.php";

    $_SESSION['id'] = $_GET['id'];

    if ( isset($_POST['cancel'] ) ) {
    	session_destroy();
        header("Location: Manager.php");
        return;
    }
   
    $sql = "SELECT * FROM POPrequestLog_ForWill WHERE id = ?";
	$params = array($_GET['id']);
	$stmt = sqlsrv_query($conn, $sql, $params);
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
	    if (isset($_POST['Auditors'])){
	    	$auditor = htmlentities($row['auditAssignment']);
	    }
		if ($row['dueDate'] == NULL){
			$ddate = "Due Date Not Set";
		}else{
			$ddate = htmlentities($row['dueDate']->format('Y-m-d'));
		}
		$bac = htmlentities($row['bac']);
		$division = htmlentities($row['division']);
		$id = htmlentities($row['id']);
	} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Edit</title>
</head>
<body>
	<h1>Manager Assign</h1>
	<?php
		if ( isset($_SESSION['error']) ) {
    		echo('<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n");
    		unset($_SESSION['error']);
		}
	?>
	<form method="POST" action="assignemail.php">
		<fieldset>
			<legend>Assign Package</legend>
				<p>Auditor:
            	<select name='Auditors'>
            	<option value="" disabled selected>Select an Auditor</option>
            	<?php
	            	$sql = "SELECT Initials, Name_Full FROM POPRequestLog_Auditors WHERE Eligible_Auditor = 'Y'";
	        		$stmt = sqlsrv_query($conn, $sql);
	        		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
	        			echo("<option value='". $row['Initials'] ."'>". $row['Name_Full'] ."</option>");
	        		}
            	?>
            	</select></p>
                <p>BAC:
				<input type="text" name="bac" size="60" value="<?= $bac ?>"/></p>
				<p>Division:
				<input type="text" name="division" size="60" value="<?= $division ?>"/></p>
				<p>Due Date:
				<input type="text" name="ddate" size="60" value="<?= $ddate ?>" id="ddate"/></p>
				<input type="hidden" name="id" value="<?= $id ?>">
				<input type="submit" name="assign" value="Assign">
		</fieldset>
	</form>
	<br></br>
	<form method="POST">
		<input type="submit" name="cancel" value="Cancel">
	</form>
</body>
</html>