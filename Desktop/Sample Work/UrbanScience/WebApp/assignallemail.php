<?php
	session_start();
	require_once "sqlsrv.php";

 	if ( isset($_POST['cancel'] ) ) {
        header("Location: Manager.php");
        return;
    }

	$email[] = array();
	if (isset($_POST['Auditors1'])) {
		$sql = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params = array($_POST['Auditors1']);
		$stmt = sqlsrv_query($conn, $sql, $params);
		while (($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) ){
			$email[0] = $row['Email'];
		}
	}
	if (Isset($_POST['Auditors2'])) {
		$sql1 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params1 = array($_POST['Auditors2']);
		$stmt1 = sqlsrv_query($conn, $sql1, $params1);
		while (($row = sqlsrv_fetch_array($stmt1, SQLSRV_FETCH_ASSOC)) ){
			$email[1] = $row['Email'];
		}
	}
	if (isset($_POST['Auditors3'])) {
		$sql2 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params2 = array($_POST['Auditors3']);
		$stmt2 = sqlsrv_query($conn, $sql2, $params2);
		while (($row = sqlsrv_fetch_array($stmt2, SQLSRV_FETCH_ASSOC)) ){
			$email[2] = $row['Email'];
		}
	}
	if (isset($_POST['Auditors4'])) {
		$sql3 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params3 = array($_POST['Auditors4']);
		$stmt3 = sqlsrv_query($conn, $sql3, $params3);
		while (($row = sqlsrv_fetch_array($stmt3, SQLSRV_FETCH_ASSOC)) ){
			$email[3] = $row['Email'];
		}
	}
	if (isset($_POST['Auditors5'])) {
		$sql4 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params4 = array($_POST['Auditors5']);
		$stmt4 = sqlsrv_query($conn, $sql4, $params4);
		while (($row = sqlsrv_fetch_array($stmt4, SQLSRV_FETCH_ASSOC)) ){
			$email[4] = $row['Email'];
		}
	}
	if (isset($_POST['Auditors6'])) {
		$sql5 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params5 = array($_POST['Auditors6']);
		$stmt5 = sqlsrv_query($conn, $sql5, $params5);
		while (($row = sqlsrv_fetch_array($stmt5, SQLSRV_FETCH_ASSOC)) ){
			$email[5] = $row['Email'];
		}
	}
	if (isset($_POST['Auditors7'])) {
		$sql6 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params6 = array($_POST['Auditors7']);
		$stmt6 = sqlsrv_query($conn, $sql6, $params6);
		while (($row = sqlsrv_fetch_array($stmt6, SQLSRV_FETCH_ASSOC)) ){
			$email[6] = $row['Email'];
		}
	}
	if (isset($_POST['Auditors8'])) {
		$sql7 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params7 = array($_POST['Auditors8']);
		$stmt7 = sqlsrv_query($conn, $sql7, $params7);
		while (($row = sqlsrv_fetch_array($stmt7, SQLSRV_FETCH_ASSOC)) ){
			$email[7] = $row['Email'];
		}
	}
	if (isset($_POST['Auditors9'])) {
		$sql8 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params8 = array($_POST['Auditors9']);
		$stmt8 = sqlsrv_query($conn, $sql8, $params8);
		while (($row = sqlsrv_fetch_array($stmt8, SQLSRV_FETCH_ASSOC)) ){
			$email[8] = $row['Email'];
		}
	}
	if (isset($_POST['Auditors10'])) {
		$sql9 = "SELECT Email FROM POPRequestLog_Auditors WHERE Initials = ?";
		$params9 = array($_POST['Auditors10']);
		$stmt9 = sqlsrv_query($conn, $sql9, $params9);
		while (($row = sqlsrv_fetch_array($stmt9, SQLSRV_FETCH_ASSOC)) ){
			$email[9] = $row['Email'];
		}
	}

	$sql = "SELECT bac, division FROM POPrequestLog_ForWill WHERE uploadDate IS NULL ORDER BY dueDate DESC";
    $stmt = sqlsrv_query($conn, $sql);
    $i = 1;
    $msg = '';
    $to = '';
	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		$to = implode(", ", $email); //replace with $_POST['email'], you can add more emails with comma
		$subject = 'Audit Assigned';
		while ( $i < $_SESSION['count']){
			$msg .= 'The MarketPlus Package with BAC number ' . $_POST['bac'.$i] . ' ' . $_POST['division'.$i] . ' has been assigned to '. $_POST['Auditors'.$i] .', and this package is due ' . $_POST['dueDate'.$i] . '.
';
			$i++;
		}
	}

	$j = 1;
	while( $j < $_SESSION['count']) {
		if ( isset($_POST['Auditors'.$j]) && isset($_POST['bac'.$j])
	    && isset($_POST['dueDate'.$j]) && isset($_POST['division'.$j])){
	    	$sql100 = "UPDATE POPrequestLog_ForWill SET auditAssignment = ? WHERE dueDate = ? AND bac = ? AND division = ?";
		    $params100 = array($_POST['Auditors'.$j], $_POST['dueDate'.$j], $_POST['bac'.$j], $_POST['division'.$j]);
		    $stmt100 = sqlsrv_query($conn, $sql100, $params100);
	    }
		$j++;
	}


	$from = 'kyle.hopper@channelvantage.com';
	$headers = 'From:'.$from;
				
	mail($to, 'Audit Assigned', $msg, $headers);
	echo "Email sent";
?>
