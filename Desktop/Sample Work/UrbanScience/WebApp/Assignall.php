<?php
    session_start();
    require_once "sqlsrv.php";

    if ( isset($_POST['cancel'] ) ) {
        session_destroy();
        header("Location: Manager.php");
        return;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Assign All</title>
</head>
<body>
	<h1>Assign All</h1>
	<?php
		$sql = "SELECT bac, division, dueDate FROM POPrequestLog_ForWill WHERE uploadDate IS NULL AND auditAssignment is NULL ORDER BY dueDate DESC";
        $stmt = sqlsrv_query($conn, $sql);
        $i = 1;
		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
			echo("<form action='assignallemail.php' method='POST'>");
			echo("<fieldset>");
			echo("<legend>Package</legend>");
			echo("<p>BAC: <input type='text' size='40' value=". $row['bac'] ." name='bac". $i ."'></p>");
            echo("<p>Division: <input type='text' size='40' value=". $row['division'] ." name='division". $i ."'></p>");
            echo("<p>Due Date: <input type='text' size='40' value=".$row['dueDate']->format('Y-m-d') ." name='dueDate". $i ."'></p>");
            echo("<p>Auditor: <select name='Auditors".$i."''>");
            $sql1 = "SELECT Initials, Name_Full FROM POPRequestLog_Auditors WHERE Eligible_Auditor = 'Y'";
	        $stmt1 = sqlsrv_query($conn, $sql1);
	        echo("<option value='' disabled selected>Select an Auditor</option>");
	        while( $row1 = sqlsrv_fetch_array( $stmt1, SQLSRV_FETCH_ASSOC) ) {
	        	echo("<option value='". $row1['Initials'] ."'>". $row1['Name_Full'] ."</option>");
	        }
            echo("</select></p>");
            echo("</fieldset>");
            $i++;
		}
		$_SESSION['count'] = $i;
		echo("<input type='submit' name='assign' value='Assign'>");
		echo("<input type='submit' name='cancel' value='Cancel'>");
        echo("</form>");
	?>
</body>
</html>