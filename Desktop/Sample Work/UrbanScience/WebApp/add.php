<?php
    session_start();
    require_once "sqlsrv.php";

    if ( isset($_POST['cancel'] ) ) {
        session_destroy();
        header("Location: Manager.php");
        return;
    }

    if ( isset($_POST['auditor']) && isset($_POST['initials']) && isset($_POST['email'])) {
	    $auditor = $_POST['auditor'];
	    $initials = $_POST['initials'];
	    $email = $_POST['email'];
	    $eligible = 'Y';
	    $sql = "INSERT INTO POPRequestLog_Auditors (Name_Full, Initials, Email, Eligible_Auditor) VALUES (?, ?, ?, ?)";
		$params = array($auditor, $initials, $email, $eligible);
	    $stmt = sqlsrv_query($conn, $sql, $params);
	    $_SESSION['success'] = 'Auditor Created';
	    header("Location: Manager.php");
	    return;
	}
 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Add Auditor</title>
</head>
	<h1>Adding Auditor</h1>
	<form method="POST">
        <fieldset>
            <legend>Create New Auditor</legend>
                <p>Auditor Name:
				<input type="text" name="auditor" size="60"/></p>
				<p>Auditor Initials:
				<input type="text" name="initials" size="60"/></p>
				<p>Auditor Email:
				<input type="email" name="email" size="60"/></p>
                <input type="submit" value="Create" name="create">
                <input type="submit" name="cancel" value="Cancel">
        </fieldset>
    </form>
</body>
</html>