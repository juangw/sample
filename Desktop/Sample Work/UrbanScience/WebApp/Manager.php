<?php
    session_start();
    require_once "sqlsrv.php";

    function validateDate($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

    if ( isset($_POST['cancel'] ) ) {
        session_destroy();
        header("Location: Manager.php");
        return;
    }

    if ( isset($_POST['delete'] ) ) {
        session_destroy();
        header("Location: delete.php");
        return;
    }

    if ( isset($_POST['add'] ) ) {
        session_destroy();
        header("Location: add.php");
        return;
    }

    if ( isset($_POST['assignall'] ) ) {
        session_destroy();
        header("Location: Assignall.php");
        return;
    }

    if ( isset($_POST['start']) && isset($_POST['end']) ){
        if (validateDate($_POST['start']) == FALSE || validateDate($_POST['end']) == FALSE) {
            $_SESSION['error'] = "Please enter the date in this format xxxx-xx-xx (date, month, day)";
            header("Location: Manager.php");
            return;
        }else{
            $_SESSION['start'] = $_POST['start'];
            $_SESSION['end'] = $_POST['end'];
            header("Location: Manager.php");
            return;
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <title>Manager</title>
</head>
<body>
	<h1>Welcome Manager</h1>
    <form method="POST">
        <fieldset>
            <legend>Add/Delete an Auditor</legend>
            <input type="submit" name="add" value="Add">
            <input type="submit" name="delete" value="Delete">
        </fieldset>
    </form>
    <br></br>
    <?php
        if ( isset($_SESSION['error']) ) {
            echo('<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n");
            unset($_SESSION['error']);
        }
        if ( isset($_SESSION['success']) ) {
                echo('<p style="color: green;">'.htmlentities($_SESSION['success'])."</p>\n");
                unset($_SESSION['success']);
        }
    ?>
	<form action="Manager.php" method="POST">
        <fieldset>
            <legend>Filter by Due Date</legend>
                <script>
                    $( function() {
                        $( "#start" ).datepicker({ dateFormat: 'yy-mm-dd'});
                    } );
                    $( function() {
                        $( "#end" ).datepicker({ dateFormat: 'yy-mm-dd'});
                    } );
                </script>
                <p>Start Date:
                <input type="text" size="40" id="start" name="start"></p>
                <p>End Date:
                <input type="text" size="40" value="<?= date("Y-m-d") ?>" id="end" name="end"></p>
                <input type="submit" value="Filter" name="filter">
                <input type="submit" name="cancel" value="Cancel">
        </fieldset>
    </form>
    <br></br>
    <?php
        if ( !empty($_SESSION['start']) && !empty($_SESSION['end']) ){
            $start = $_SESSION['start'];
            $end = $_SESSION['end'];
            $sql = ("SELECT * FROM POPrequestLog_ForWill WHERE uploadDate IS NULL AND dueDate BETWEEN ? AND ? ORDER BY dueDate DESC");
            $params = array($start, $end);
            $stmt = sqlsrv_query($conn, $sql, $params);
            echo("<table border='1'>");
            echo("<thead><tr><th>BAC</th><th>Division</th><th>Auditor</th><th>Due Date</th><th>Complete Date</th><th>Upload Date</th><th>Update</th></tr></thead>");
            while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
                echo("<thead></thead>");
                echo("</td><td>");
                echo($row['bac']);
                echo("</td><td>");
                echo($row['division']);
                echo("</td><td>");
                echo($row['auditAssignment']);
                echo("</td><td>");
                if ($row['dueDate'] == NULL){
                    echo("No Due Date Set");
                }else{
                    echo($row['dueDate']->format('Y-m-d'));
                }
                echo("</td><td>");
                if ($row['auditCompleteDate'] == NULL){
                    echo("Not Completed Yet");
                }else{
                    echo($row['auditCompleteDate']->format('Y-m-d'));
                }
                echo("</td><td>");
                if ($row['uploadDate'] == NULL){
                    echo("Not Uploaded Yet");
                }else{
                    echo(htmlentities($row['uploadDate']->format('Y-m-d')));
                }
                echo("</td><td>");
                echo('<a href="Managerassign.php?id='.htmlentities($row['id']).'">Assign</a> / ');
                echo('<a href="Managerapprove.php?id='.htmlentities($row['id']).'">Approve</a> / ');
                echo('<a href="Managerupload.php?id='.htmlentities($row['id']).'">Upload</a>');
            }
            echo("</table>");
        }else{
            $sql = "SELECT * FROM POPrequestLog_ForWill WHERE uploadDate IS NULL ORDER BY dueDate DESC";
            $stmt = sqlsrv_query($conn, $sql);
                echo("<table border='1'>");
                echo("<thead><tr><th>BAC</th><th>Division</th><th>Auditor</th><th>Due Date</th><th>Complete Date</th><th>Upload Date</th><th>Update</th></tr></thead>");
                while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
                    echo("<thead></thead>");
                    echo("</td><td>");
                    echo($row['bac']);
                    echo("</td><td>");
                    echo($row['division']);
                    echo("</td><td>");
                    echo($row['auditAssignment']);
                    echo("</td><td>");
                    if ($row['dueDate'] == NULL){
                        echo("No Due Date Set");
                    }else{
                        echo($row['dueDate']->format('Y-m-d'));
                    }   
                    echo("</td><td>");
                    if ($row['auditCompleteDate'] == NULL){
                        echo("Not Completed Yet");
                    }else{
                        echo($row['auditCompleteDate']->format('Y-m-d'));
                    }
                    echo("</td><td>");
                    if ($row['uploadDate'] == NULL){
                        echo("Not Uploaded Yet");
                    }else{
                        echo(htmlentities($row['uploadDate']->format('Y-m-d')));
                    }
                    echo("</td><td>");
                    echo('<a href="Managerassign.php?id='.htmlentities($row['id']).'">Assign</a> / ');
                    echo('<a href="Managerapprove.php?id='.htmlentities($row['id']).'">Approve</a> / ');
                    echo('<a href="Managerupload.php?id='.htmlentities($row['id']).'">Upload</a>');
                }
            echo("</table>");
            echo("<form method = 'POST'>");
            echo("<input type='submit' value='Assign All' name='assignall'>");
            echo("</form>");
            }
    ?>
</body>
</html>