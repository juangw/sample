<?php
    session_start();
    require_once "sqlsrv.php";

    if ( isset($_POST['cancel'] ) ) {
        session_destroy();
        header("Location: Manager.php");
        return;
    }

    if (isset($_POST['delete'])) {
	    $name = $_POST['Auditors'];
	    $sql = "UPDATE POPRequestLog_Auditors SET Eligible_Auditor = 'N' WHERE Initials = ?";
		$params = array($name);
	    $stmt = sqlsrv_query($conn, $sql, $params);
	    $_SESSION['success'] = 'Auditor Deleted';
	    header("Location: Manager.php");
	    return;
	}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <title>Delete Auditor</title>
</head>
<body>
	<h1>Deleting Auditor</h1>
	<form method="POST">
        <fieldset>
            <legend>Choose an Auditor</legend>
            	<p>Auditor:
            	<select name='Auditors'>
            	<option value="" disabled selected>Select an Auditor</option>
            	<?php
	            	$sql = "SELECT Initials, Name_Full FROM POPRequestLog_Auditors WHERE Eligible_Auditor = 'Y'";
	        		$stmt = sqlsrv_query($conn, $sql);
	        		while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
	        			echo("<option value='". $row['Initials'] ."'>". $row['Name_Full'] ."</option>");
	        		}
            	?>
            	</select></p>
            	<input type="submit" value="Delete" name="delete">
            	<input type="submit" name="cancel" value="Cancel">
        </fieldset>
    </form>
</body>
</html>