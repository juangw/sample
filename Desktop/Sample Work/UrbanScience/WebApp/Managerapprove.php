<?php
    session_start();
    require_once "sqlsrv.php";

    if ( isset($_POST['cancel'] ) ) {
        header("Location: Manager.php");
        return;
    }

    $sql = "SELECT * FROM POPrequestLog_ForWill WHERE id = ?";
	$params = array($_GET['id']);
    $stmt = sqlsrv_query($conn, $sql, $params);
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
		$auditor = htmlentities($row['auditAssignment']);
		$ddate = htmlentities($row['dueDate']->format('Y-m-d'));
		if ($row['auditCompleteDate'] == NULL){
			$cdate = "No Completion Date";
		}else{
			$cdate = htmlentities($row['auditCompleteDate']->format('Y-m-d'));
		}
		$bac = htmlentities($row['bac']);
		$division = htmlentities($row['division']);
		$comments = htmlentities($row['comments']);
		$id = htmlentities($row['id']);
    }

    if (isset($_POST['approve'])){
    	$sql = "UPDATE POPrequestLog_ForWill SET auditCompleteDate = ? WHERE id = ?";
    	$params = array($_POST['cdate'], $_GET['id']);
    	$stmt = sqlsrv_query($conn, $sql, $params);
    	$_SESSION['success'] = 'Package Approved';
    	header( 'Location: Manager.php');
    	return;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="style.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
    <title>Edit</title>
</head>
<body>
	<h1>Manager Approval</h1>
	<?php
		if ( isset($_SESSION['error']) ) {
    		echo('<p style="color: red;">'.htmlentities($_SESSION['error'])."</p>\n");
    		unset($_SESSION['error']);
		}
	?>
	<form method="POST">
		<fieldset>
			<legend>Approve Package</legend>
				<p>Auditor:
				<input type="text" name="auditor" size="60" value="<?= $auditor ?>"/></p>
				<p>BAC:
				<input type="text" name="bac" size="60" value="<?= $bac ?>"/></p>
				<p>Division:
				<input type="text" name="division" size="60" value="<?= $division ?>"/></p>
				<p>Due Date:
				<input type="text" name="ddate" size="60" value="<?= $ddate ?>"/></p>
				<script>
                    $( function() {
                        $( "#cdate" ).datepicker({ dateFormat: 'yy-mm-dd'});
                    } );
                </script>
				<p>Complete Date:
				<input type="text" name="cdate" size="60" value="<?= $cdate ?>" id="cdate"/></p>
				<p>Comments:
				<textarea rows="6" cols="50" name="comments" value=""><?= $comments ?></textarea></p>
				<input type="hidden" name="id" value="<?= $id ?>">
				<input type="submit" name="approve" value="Approve">
				<input type="submit" name="cancel" value="Cancel">
		</fieldset>
	</form>
</body>
</html>